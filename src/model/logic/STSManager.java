package model.logic;

import API.ISTSManager;
import model.data_structures.IList;
import model.vo.VOParada;
import model.vo.VORetardo;
import model.vo.VORuta;
import model.vo.VOServicio;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VOTransfer;
import model.vo.VOViaje;

public class STSManager implements ISTSManager
{

	@Override
	public void ITSInit() 
	{
		// TODO Auto-generated method stub	
	}

	@Override
	public void ITScargarGTFS() 
	{
		// TODO Auto-generated method stub	
	}

	@Override
	public void ITScargarTR(String fecha) 
	{
		// TODO Auto-generated method stub	
	}

	@Override
	public IList<VORuta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOViaje> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParada> ITSparadasRetrasadasFecha(String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOTransfer> ITStransbordosRuta(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOPlan ITSrutasPlanUtilizacion(IList<String> idsDeParadas, String fecha, String horaInicio, String horaFin) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VORuta> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOViaje> ITSviajesRetrasoTotalRuta(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VORangoHora ITSretardoHoraRuta(String idRuta, String Fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOViaje> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VORuta ITSrutaMenorRetardo(String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOServicio> ITSserviciosMayorDistancia(String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VORetardo> ITSretardosViaje(String fecha, String idViaje) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParada> ITSparadasCompartidas(String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
